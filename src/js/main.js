import $ from "jquery";
import app from "./components/vue/calculator.js";
import "./components/arrow-animations.js";




$(document).ready(function () {
	
	$('.header-menu_drop-btn').on('click', function () {
		console.log('click')

		$('.header-menu-list').toggleClass('open')
		
	})

	$('.top-line-nav_drop-btn').on('click', function () {
		$('.top-line-nav-holder').slideToggle(300, function () {
			if ($(this).css('display') === 'none') {
				$(this).removeAttr('style')
			}
		})
	})

	$('.lang__btn').on('click', function () {
		$('.lang__drop').toggleClass('open');
	})


	$('.js-open-answer').on('click', function (e) {
		let title = $(this).find('.questions-item__question');
		let answer = $(this).find('.js-answer');
		if ($(e.target).is('.js-hide')) {
			title.removeClass('is-open')
			answer.slideUp(300)
		}

		if (!$(e.target).is('.questions-item__question')) return
		title.addClass('is-open')
		answer.slideDown(300)
		
	})

})
let hideBtn = $(this).find('.js-hide');




import $ from "jquery";
import {TimelineMax,TweenMax} from 'gsap';


//		js-link-anim
//		js-text
//		js-arrow

$(document).ready(function () {
	

	$('.js-link-anim').on('mouseenter', function (e) {
		let arrow = $(this).find('.js-arrow');
		let fakeArrow = $(this).find('.js-arrow-fake');
		let text = $(this).find('.js-text');
		let tl = new TimelineMax();

		tl.to(arrow, .4, {x: 200, opacity: 0, ease: Back.easeInOut.config(1.7)})
		  .to(text, .4, {x: 50, ease: Back.easeInOut.config(1.7)}, 0)
		  .to(fakeArrow, .4, {left: -20, opacity: 1, ease: Back.easeInOut.config(1)}, 0)
		  .delay(0.1)

		// tl.to(arrow, .3, {x: 200, opacity: 0})
		//   .to(text, .3, {x: 50}, 0)
		//   .to(fakeArrow, .3, {left: -20, opacity: 1}, 0)
		//   .delay(0.1)
	})

	$('.js-link-anim').on('mouseleave', function (e) {
		let arrow = $(this).find('.js-arrow');
		let fakeArrow = $(this).find('.js-arrow-fake');
		let text = $(this).find('.js-text');
		let tl = new TimelineMax();

		tl.to(arrow, .4, {x: 0, opacity: 1, ease: Back.easeInOut.config(1.7)}, 0)
		  .to(text, .4, {x: 0, ease: Back.easeInOut.config(1.7)}, 0)
		  .to(fakeArrow, .4, {left: -300, opacity: 0, ease: Back.easeInOut.config(1)}, 0)
		  // .delay(0.1)

		// tl.to(arrow, .3, {x: 0, opacity: 1}, 0)
		//   .to(text, .3, {x: 0}, 0)
		//   .to(fakeArrow, .3, {left: -300, opacity: 0}, 0)
		//   .delay(0.1)
	})

})


	// $('.js-link-anim').on('mouseover', function (e) {
		// let arrow = $(this).find('.js-arrow');
		// let text = $(this).find('.js-text');
		// console.log('arrow', arrow)
		// console.log('text', text)
		// // let tl = new TweenMax();
		// let tl = new TimelineMax();

		// tl.to(arrow, .3, {x: 200, opacity: 0})
		//   .to(arrow, 0, {x: -300, opacity: 0})
		//   .to(text, .3, {x: 50}, 0)
		//   .to(arrow, .3, {x: -200, opacity: 1}, '+=0.1')
		//   // .to(arrow, 0.4, {x: -10, opacity: 1}, 0)
	// }) 

	// $('.js-link-anim').on('mouseout', function (e) {
	// 	let arrow = $(this).find('.js-arrow');
	// 	let text = $(this).find('.js-text');
	// 	console.log('arrow', arrow)
	// 	console.log('text', text)
	// 	// let tl = new TweenMax();
	// 	let tl = new TimelineMax();

	// 	tl.to(arrow, .3, {x: -300, opacity: 0})
	// 	  .to(arrow, 0, {x: 300, opacity: 0})
	// 	  .to(text, .3, {x: 0}, 0)
	// 	  .to(arrow, .3, {x: 0, opacity: 1}, '-=0.1')

	// })
import Vue from "vue/dist/vue.js";
let vueElem = document.querySelector('.offerlist-block');
import offerlistData from "./offerlist.js";

console.log('offerlistData',offerlistData);
let app = new Vue({
	el: vueElem == null ? false : vueElem,
	data: {
		offerlist: [],
		quantity: 1,
		countOfQuantitys: 20,
		itemToShow: {}
	},
	computed: {
		quantityIndexing: function () {
			let obj = {};
			let el = this.itemToShow;
			obj.countOfYears = +el.countOfYears;
			obj.savings = +el.savings * this.quantity;
			obj.total = +el.total * this.quantity * obj.countOfYears;
			return obj;
		}
	},
	created() {
		this.offerlist = offerlistData;
		let self = this;
		this.offerlist.forEach(function (el) {
			if (el.active) {
				self.itemToShow = el;
			}
		})


		window.addEventListener('keydown', function (e) {
			//console.log(e)
			if (e.code == 'Space') {
				console.log(self.quantityIndexing)
			}
		})
	},
	methods: {
		yearText: function (el) {
			if (el.countOfYears > 1) {
				return 'Years'
			} else {
				return 'Year'
			}
		},

		getSavingAmount(el) {
			let res = el.oldprice - (el.total*el.countOfYears);
			el.savings = res.toFixed(2);
			return res = res.toFixed(2);
		},

		getSavingPercent(el) {
			let percentRes = (100 - (el.total*el.countOfYears * 100 / el.oldprice)).toFixed(0);
			el.savingsPercent = percentRes;
			return percentRes;
		},

		getId(i) {
			return 'chk' + i;
		},

		setActive(item) {
			this.offerlist.forEach(function (el) {
				el.active = false;
			});
			item.active = true;
			this.itemToShow = item;
		}
		
	}
})

export default app;
let offerlistData = [
	{	
		countOfYears: 1,
		total: 35.00,
		oldprice: 99.95,
		active: true,
		savings: null,
		savingsPercent: null
	},
	{	
		countOfYears: 2,
		total: 30.63,
		oldprice: 177.90,
		active: false,
		savings: null,
		savingsPercent: null
	},
	{	
		countOfYears: 3,
		total: 29.17,
		oldprice: 299.85,
		active: false,
		savings: null,
		savingsPercent: null
	},
	{	
		countOfYears: 4,
		total: 28.44,
		oldprice: 399.80,
		active: false,
		savings: null,
		savingsPercent: null
	},
]

export default offerlistData;